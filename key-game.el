;;; key-game.el --- Create key-based tutorial games -*- lexical-binding: t -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 1.0.1
;; Package-Requires: ((emacs "25.1"))
;; File: key-game.el
;; Keywords: games
;; URL: https://gitlab.com/grinn.amy/key-game

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Create a tutorial game for your Emacs package.  Start by defining a
;; game with the `key-game' macro.  The DOC-STRING and TITLE keyword
;; are required parameters.  This will define a `NAME-level' macro,
;; where NAME is the name of the `key-game'.  Use this macro to define
;; levels for the game.
;;
;; For example, for a `key-game' named "example":
;;
;;   (key-game example
;;     "An example game."
;;     :title "example")
;;
;; The macro to define levels for this game will be named
;; `example-level':
;;
;;   (example-level level-1
;;     "Level 1: intro")
;;
;; Within the level creation macro, define `frame's and, optionally,
;; an `intro':
;;
;;   (example-level level-1
;;     "Level 1: intro"
;;     (intro
;;      "This is an intro.")
;;     (frame
;;      :keys ("x")
;;      "Press the `x' key: "
;;      (goto-char (point-max)))
;;     (frame
;;      :commands (next-line)
;;      "Go to the next line\n"))
;;
;; See the documentation for `key-game-frame' for all options for
;; both `intro's and `frame's.

;;; Code:

;;;; Requirements

(require 'cl-lib)
(require 'easy-mmode)
(require 'eieio)

;;;; Types:

(defclass key-game--level ()
  ((name
    :initarg :name
    :type symbol
    :documentation "The symbol corresponding to the current level.")
   (next
    :initarg :next
    :type symbol
    :documentation "The symbol corresponding to the next level.")
   (random
    :initarg :random
    :type boolean
    :documentation "Whether or not to display frames in a random order.")
   (frames
    :initarg :frames
    :type list
    :initform nil
    :documentation "A list of symbols corresponding to frames within the current
level.")
   (completed
    :initarg :completed
    :initform nil
    :type list
    :documentation "A list of `key-game-frame's representing completed frames."))
  "A representation of a level in the key game.")

(defclass key-game--frame ()
  ((name
    :initarg :name
    :type symbol
    :documentation "A symbol corresponding to the current frame.")
   (no-stats
    :initarg :no-stats
    :type boolean
    :documentation "Whether to show stats in the header-line")
   (commands
    :initarg :commands
    :type list
    :documentation "A list of commands a user must enter.")
   (keys
    :initarg :keys
    :type list
    :documentation "A list of keys a user must enter.")
   (condition
    :initarg :condition
    :documentation "A condition that must return t after the
 commands or keys for this frame are satisfied.")
   (completed
    :initarg :completed
    :initform nil
    :type boolean
    :documentation "Whether this frame is completed.")
   (attempts
    :initarg :attempts
    :initform 0
    :type integer
    :documentation "The number of failed attempts for this frame."))
  "A representation of a frame in the key game.")

;;;; Variables:

(defvar-local key-game-prefix "C-c"
  "Prefix key for key game commands.")
(put 'key-game-prefix 'permanent-local t)

(defvar-local key-game-title "Key-based Tutorial Game"
  "Title for the key game.")
(put 'key-game-title 'permanent-local t)

(defvar-local key-game-default-mode 'fundamental-mode
  "Default mode for key game frames.

Can be overridden for individual frames with the :mode option.")
(put 'key-game-default-mode 'permanent-local t)

(defvar-local key-game-setup nil
  "Setup function for every frame.")
(put 'key-game-setup 'permanent-local t)

(defvar-local key-game--current-frame nil
  "Current frame being displayed.")
(put 'key-game--current-frame 'permanent-local t)

(defvar-local key-game--current-level nil
  "Current level being played.")
(put 'key-game--current-level 'permanent-local t)

(defvar-local key-game--menu nil
  "Function to return to the main menu.")
(put 'key-game--menu 'permanent-local t)

(defvar-local key-game--overlay nil
  "Overlay for messages in the key game.")
(put 'key-game--overlay 'permanent-local t)

(defvar-local key-game--skip-command nil
  "Whether to skip evaluating frame for next command.")

(defvar-local key-game--continue nil
  "Function to continue to the next frame/level.")


;;;; Font lock

(defun key-game-font-lock-keywords (bound)
  "Syntax highlighting within level definitions for `key-game's.

BOUND is the limit of the search for keywords."
  (when (re-search-forward "\\_<\\(?:frame\\|intro\\)\\_>" bound t)
    (let ((data (match-data))
          (container (ignore-errors
                       (save-excursion (up-list -2) (forward-char)
                                       (intern (thing-at-point 'symbol))))))
      (when (and container (function-get container 'key-game))
        (set-match-data data)
        t))))

(font-lock-add-keywords 'emacs-lisp-mode '(key-game-font-lock-keywords))

;;;; Header line:

(defun key-game-header--stats ()
  "Statistics about the current level."
  (if (and key-game--current-level key-game--current-frame
           (not (oref key-game--current-frame no-stats)))
      (with-slots ((completed-frames completed)
                   (outstanding-frames frames))
          key-game--current-level
        (with-slots ((current-frame name)
                     (current-attempts attempts)
                     (current-frame-completed completed))
            key-game--current-frame
          (concat (mapconcat (lambda (frame)
                               (if frame
                                   (if (<= (oref frame attempts) 0)
                                       "."
                                     "x")))
                             (reverse completed-frames)
                             "")
                  (if (member current-frame outstanding-frames)
                      (if (= 0 current-attempts)
                          (if current-frame-completed
                              "."
                            "o")
                        "x")
                    (if outstanding-frames
                        "-"))
                  (make-string (max 0 (1- (length outstanding-frames))) ?-))))))

(defun key-game-header--center ()
  "Centered text for the key game header."
  (concat (propertize " "
                      'display `(space :align-to (- center ,(/ (length key-game-title) 2))))
          key-game-title))

(defun key-game-header--right ()
  "Right-aligned text for the key game header."
  (let ((text (or (and key-game--current-level
                       (documentation (oref key-game--current-level name))))))
    (concat (propertize " "
                        'display `(space :align-to (- right ,(length text))))
            text)))

;;;; Key game mode:

(defun key-game-continue ()
  "Continue to the next level."
  (interactive)
  (if key-game--continue
      (funcall key-game--continue)))

(defun key-game-menu ()
  "Return to the main menu."
  (interactive)
  (if key-game--menu
      (funcall key-game--menu)))

(defun key-game-restart ()
  "Restart the current level."
  (interactive)
  (if key-game--current-level
      (funcall (oref key-game--current-level name))))

(defun key-game--post-command-function ()
  "React to the current win state of the current frame."
  (when key-game--current-frame
    (if key-game--skip-command
        (setq key-game--skip-command nil)
      (with-slots (commands keys completed attempts) key-game--current-frame
        (when (and (not completed) (or commands keys))
          (pcase (key-game--check-frame)
            ('partial)
            ('t
             (setq completed t)
             (key-game--success))
            ('nil
             (setq completed t)
             (cl-incf attempts)
             (key-game--failure))))))))

(defvar key-game-mode-map (make-sparse-keymap)
  "Keymap for key games.")

(defvar key-game-command-map
  (easy-mmode-define-keymap
   (mapcar (lambda (k) (cons (kbd (car k)) (cdr k)))
           '(("q" . quit-window)
             ("C-q" . quit-window)
             ("C-c" . key-game-continue)
             ("C-r" . key-game-restart)
             ("r" . key-game-restart)
             ("a" . key-game-menu)
             ("C-a" . key-game-menu))))
  "Command keymap for key games.")

(define-minor-mode key-game-mode
  "Key game minor mode."
  :lighter " Key game"
  :keymap key-game-mode-map
  (if key-game-mode
      (progn
        (setcdr key-game-mode-map nil) ;; Remove previous prefix binding
        (cond
         ((null key-game-prefix)
          (define-key key-game-mode-map
                      (kbd "C-c")
                      key-game-command-map))
         ((string= key-game-prefix "")
          (setcdr key-game-mode-map (cdr key-game-command-map)))
         (t
          (define-key key-game-mode-map
                      (kbd key-game-prefix)
                      key-game-command-map)))
        (add-hook 'post-command-hook #'key-game--post-command-function nil t)
        (setq header-line-format
              '(""
                (:eval (key-game-header--stats))
                (:eval (key-game-header--center))
                (:eval (key-game-header--right)))))
    (remove-hook 'post-command-hook #'key-game--post-command-function t)
    (setq header-line-format nil)))

;;;; Macros

(cl-defmacro key-game (name doc-string &key title message prefix mode setup)
  "Create a new key game named NAME.

DOC-STRING is a string description of the game.

TITLE is required and is the name of the buffer and the center of
the header line.

MESSAGE will be displayed before the main menu.

PREFIX is a key prefix string which will launch
`key-game-command-map'.  Use an empty string to set the command
map as the current keymap.

MODE is the default major mode to use for each frame, excluding
the main menu.  Individually, each frame can specify a different
mode.  Appending \"-mode\" to this parameter is optional.

SETUP is an s-expression which will be evaluated for each frame.

This macro defines an interactive function NAME which will launch
the key game.  Additionally, it defines a macro NAME-level to add
levels to the game.

Evaluating a `key-game' form will clear all levels from the game."
  (declare (indent 1) (doc-string 2))
  (let ((make-level (intern (format "%s-level" name))))
    (setq mode (key-game--get-major-mode mode))
    `(prog1
         (defun ,name ()
           ,doc-string
           (interactive)
           (key-game--buffer-setup ',name ,title ,prefix ',mode (lambda () ,setup))
           (let ((inhibit-read-only t))
             (erase-buffer)
             (key-game--insert-menu ',name ,message)
             ,setup))
       (function-put ',name 'key-game-levels nil) ; Remove all levels
       (function-put ',make-level 'key-game t) ; Used for `font-lock-mode'
       (cl-defmacro ,make-level
           (name doc-string &rest frames &key next random &allow-other-keys)
         ,(concat "Create a new level for `" (symbol-name name) "' named NAME.

DOC-STRING will be used to create the menu entry.

NEXT should be the symbol for the next level after this one, if
one exists.

If RANDOM is non-nil, display all specified FRAMES in a random
order.

The rest of the arguments FRAMES should be either `intro' or
`frame' macro calls.  See the documentation for `key-game-frame'
for available options.  One `intro' can be specified and will
always be displayed first.  Each `frame' specified will be
displayed in the order they are defined unless RANDOM is non-nil.")
         (declare (indent 1) (doc-string 2))
         (cl-remf frames :next)
         (cl-remf frames :random)
         (let ((level (intern (format "%s-%s" ',name name)))
               (i 0)
               intro-symbol
               frame-symbols)
           (setq frames
                 (mapcar
                  (lambda (frame)
                    (let ((sym (pop frame)))
                      (cond
                       ((eq 'intro sym)
                        (setq intro-symbol (intern (format "%s-intro" level)))
                        (push intro-symbol frame))
                       ((eq 'frame sym)
                        (cl-incf i)
                        (let ((frame-sym (intern (format "%s-%d" level i))))
                          (push frame-sym frame-symbols)
                          (push frame-sym frame)))
                       (t
                        (signal 'error (format "`%s' is not one of `frame' or `intro'." sym)))))
                    (push 'key-game-frame frame))
                  frames))
           `(prog1
                (key-game-level ,level
                  ,doc-string
                  ,(if next (intern (format "%s-%s" ',name next)))
                  ,random)
              ,@frames
              (function-put ',level 'key-game-intro ',intro-symbol)
              (function-put ',level 'key-game-frames ',(nreverse frame-symbols))
              (let ((levels (function-get ',',name 'key-game-levels)))
                (unless (member ',level levels)
                  (function-put ',',name 'key-game-levels
                                (append levels (list ',level)))))))))))

(cl-defmacro key-game-frame (name &rest init
                                  &key mode commands keys condition read-only no-stats
                                  &allow-other-keys)
  "Create a frame named NAME for a key game.

When using `frame' or `intro' macros, NAME is generated
automatically and should not be specified.

MODE is the major mode for this frame, overriding the game's
default mode if it is set.  Appending \"-mode\" to this is
optional.

Either COMMANDS or KEYS can be specified for the frame.
Otherwise, a continue message will be displayed immediately.

COMMANDS are a list of command symbols or lists which should be
entered by the player in the order they are defined.  If it is a
list, the first symbol should be `or' and the rest of the list
are all valid commands for the player to enter.  For example:

:commands (next-line (or backward-word backward-sexp))

Will allow the user to navigate to the next line and either
backward a single word or backward a single s-expression in order
to complete this frame.

KEYS are a list of strings or lists which should be entered by
the player in the order they are defined.  If it is a list, the
first symbol should be `or' and the rest of the list are all
valid keys for the player to enter.  For example:

:keys ((or \"a\" \"b\") \"c\")

Will allow the user to type either `a' or `b' then finally `c' to
complete this frame.

CONDITION is an s-expression which will be evaluated after
COMMANDS or KEYS are satisfied.  If it returns a non-nil value,
the frame is considered complete, otherwise it is considered
failed.

If READ-ONLY is non-nil, the buffer will be placed in
`read-only-mode'.

If NO-STATS is non-nil, the statistics will be hidden from the
header line.

The rest of the arguments INIT are evaluated with a blank buffer.
If the first element is a string, that string will be printed to
the buffer with command names substituted with key descriptions
via `substitute-command-keys'.

For example, an intro might look like this:

\(intro
 :read-only t
 \"This is an intro message.\")

Or a challenge frame might look like this:

\(frame
 :keys (\"o\")
 \"Type the letter `o': \"
 (goto-char (point-max)))"
  (declare (indent 1))
  (dolist (k '(:mode :commands :keys :condition :read-only :no-stats))
    (cl-remf init k))
  (setq mode (key-game--get-major-mode mode))
  (if (stringp (car init))
      (push `(save-excursion
               (insert (substitute-command-keys ,(pop init))))
            init))
  `(defun ,name ()
     (funcall (or ',mode key-game-default-mode 'fundamental-mode))
     (read-only-mode ,(if read-only 1 -1))
     (key-game-mode 1)
     (if key-game-setup (funcall key-game-setup))
     (if (and key-game--current-frame
              (eq ',name (oref key-game--current-frame name)))
         (oset key-game--current-frame completed nil)
       (setq key-game--current-frame
             (key-game--frame
              :name ',name
              :no-stats ,no-stats
              :keys ',keys
              :commands ',commands
              :condition ,(if condition `(lambda () ,condition)))))
     (overlay-put key-game--overlay 'after-string nil)
     (setq key-game--continue nil)
     (setq key-game--skip-command t)
     (let ((inhibit-read-only t))
       (erase-buffer)
       ,@init)))

(defmacro key-game-level (name doc-string next random)
  "Create a new level for a key game named NAME.

DOC-STRING is the documentation for the level.  It will be used
to create a menu entry in the main menu.

NEXT is a symbol for the next level after this one, if one
exists.

If RANDOM is non-nil, display this level's frames in a random
order.  Otherwise, display them in the order they were defined."
  `(defun ,name ()
     ,doc-string
     (setq key-game--current-level
           (key-game--level
            :name ',name
            :next ',next
            :random ,random
            :frames (copy-sequence (function-get ',name 'key-game-frames))))
     (if (function-get ',name 'key-game-intro)
         (progn
           (funcall (function-get ',name 'key-game-intro))
           (setq key-game--continue 'key-game--next-frame)
           (move-overlay key-game--overlay (point-max) (point-max))
           (overlay-put key-game--overlay 'after-string
                        (substitute-command-keys
                         "\n\n\nPress \\[key-game-continue] to continue.")))
       (key-game--next-frame))))

;;;; Utility expressions:

(eval-and-compile
  (defun key-game--get-major-mode (name)
    "Get the symbol for the major mode from NAME."
    (when name
      (if (string-match-p "-mode$" (symbol-name name))
          name
        (intern (format "%s-mode" name))))))

(defun key-game--buffer-setup (name title prefix mode setup)
  "Set up a buffer for a key game.

NAME is the symbol name of the key game which will launch the
main menu.  TITLE is the name of the buffer and the center of the
`header-line'.  PREFIX is the key sequence string which should
prepend the `key-game-command-map'.  MODE is the default major
mode to use for all frames.  SETUP is a function which will be
called before each frame."
  (switch-to-buffer (get-buffer-create (format "*%s*" title)))
  (setq key-game-title title)
  (setq key-game--menu name)
  (setq key-game-prefix prefix)
  (setq key-game-default-mode mode)
  (setq key-game--current-frame nil)
  (setq key-game-setup setup)
  (fundamental-mode)
  (key-game-mode 1)
  (read-only-mode 1)
  (remove-overlays)
  (setq key-game--overlay (make-overlay 0 0)))

(defun key-game--insert-menu (game message)
  "Insert a main menu.

GAME is the symbol for the current game.  MESSAGE will be
inserted before the menu."
  (newline 2)
  (if message (insert message))
  (dolist (level (function-get game 'key-game-levels))
    (newline 2)
    (insert-button (documentation level) 'action `(lambda (_) (,level))))
  (newline 2)
  (insert (substitute-command-keys "At any time, press \\[key-game-restart] to restart the current level"))
  (newline 2)
  (insert (substitute-command-keys "You can also press \\[key-game-menu] to return to this menu at any time."))
  (goto-char (point-min)))

(defun key-game--get-keys (n)
  "Get N latest keys."
  (mapcar
   #'key-description
   (seq-subseq
    (seq-reduce
     (lambda (keys e)
       (if (listp e)
           (if (atom (cdr e))
               (push nil keys))
         (if (> (length keys) 0)
             (push e (car keys))))
       keys)
     (reverse (recent-keys t))
     nil)
    (- n))))

(defun key-game--get-commands (n)
  "Get N latest commands."
  (seq-subseq
   (seq-reduce
    (lambda (commands e)
      (if (and (listp e) (atom (cdr e)))
          (push (cdr e) commands))
      commands)
    (reverse (recent-keys t))
    nil)
   (- n)))

(defun key-game--eq (test correct)
  "Check if TEST list is equal to specification CORRECT.

CORRECT is a list equal in length to TEST.  Each element may
either be an atom, which will be compared directly, or a list
starting with `or', in which case check if TEST element is a
member of the list."
  (when (= (length test) (length correct))
    (catch 'mismatch
      (dotimes (i (length test))
        (if (listp (nth i correct))
            (unless (member (nth i test) (cdr (nth i correct)))
              (throw 'mismatch nil))
          (unless (equal (nth i test) (nth i correct))
            (throw 'mismatch nil))))
      t)))

(defun key-game--check-frame ()
  "Check `key-game--current-frame' for completion.

If the frame is complete, return t.  If partially complete,
return `partial'.  Otherwise, return nil"
  (catch 'result
    (with-slots (commands keys condition) key-game--current-frame
      (let* ((n (if commands (length commands) (length keys)))
             (lossage (if commands
                          (key-game--get-commands n)
                        (key-game--get-keys n))))
        (if (key-game--eq lossage (or commands keys))
            (throw 'result (or (not condition) (funcall condition))))
        (setq n (1- n))
        (while (> n 0)
          (if (key-game--eq
               (seq-subseq lossage (- n))
               (seq-subseq (or commands keys) 0 n))
              (throw 'result 'partial))
          (setq n (1- n)))))
    nil))

(defun key-game--success ()
  "Create a success message and allow \\<key-game-mode-map>\\[key-game-continue]."
  (move-overlay key-game--overlay (point-max) (point-max))
  (overlay-put key-game--overlay 'after-string
               (substitute-command-keys
                "\n\n\nGreat job! Press \\[key-game-continue] to continue."))
  (setq key-game--continue 'key-game--next-frame))

(defun key-game--failure ()
  "Create a failure message and allow \\<key-game-mode-map>\\[key-game-continue]."
  (with-slots (attempts name) key-game--current-frame
    (move-overlay key-game--overlay (point-max) (point-max))
    (overlay-put key-game--overlay 'after-string
                 (concat
                  (substitute-command-keys
                   "\n\n\nNot quite. Press \\[key-game-continue] to try again.")
                  (if (> attempts 2)
                      (key-game--hint))))
    (setq key-game--continue name)))

(defun key-game--hint ()
  "Get a hint for the current frame."
  (with-slots (commands keys) key-game--current-frame
    (concat "\n\nHint: "
            (mapconcat
             (lambda (c)
               (propertize (format "%s" c) 'font-lock-face 'help-key-binding))
             (or keys commands)
             ", "))))

(defun key-game--next-frame ()
  "Call the next frame in the current level.

Calls the next level if no more frames exist."
  (let ((current-frame (and key-game--current-frame
                            (oref key-game--current-frame name)))
        (level-frames (oref key-game--current-level frames)))
    (when (member current-frame level-frames)
      (setq level-frames (delq current-frame level-frames))
      (oset key-game--current-level frames level-frames)
      (object-add-to-list key-game--current-level :completed key-game--current-frame))
    (if level-frames
        (if (oref key-game--current-level random)
            (funcall (nth (random (length level-frames)) level-frames))
          (funcall (car level-frames)))
      (key-game--level-completed))))

(key-game-frame key-game--level-completed
  :mode fundamental
  :read-only t
  (insert "\n\nCongratulations!\n\nYou've completed "
          (documentation (oref key-game--current-level name))
          (substitute-command-keys "\n\nPress \\[key-game-continue] to "))
  (if (oref key-game--current-level next)
      (progn
        (setq key-game--continue (oref key-game--current-level next))
        (insert "continue to the next level."))
    (setq key-game--continue 'key-game-menu)
    (insert "return to the main menu"))
  (insert (substitute-command-keys "\n\nPress \\[key-game-restart] to restart this level.")
          (substitute-command-keys "\n\nPress \\[key-game-menu] to return to the main menu."))
  (goto-char (point-min)))

(provide 'key-game)

;;; key-game.el ends here
